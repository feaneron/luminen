# Luminen

Control LED and light panels connected to your local network.

Supported devices:

 * Elgato Key Light
 * Elgato Key Light Air
 * Elgato Key Light Mini
 * Elgato Light Stripe (only brightness and temperature)
 * Elgato Ring Light
