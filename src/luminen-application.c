/* luminen-application.c
 *
 * Copyright 2023 Georges Stavracas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "luminen-application.h"
#include "luminen-window.h"
#include <libpeas.h>

struct _LuminenApplication
{
	AdwApplication parent_instance;
};

G_DEFINE_TYPE (LuminenApplication, luminen_application, ADW_TYPE_APPLICATION)

/*
 * Auxiliary methods
 */

static void
load_plugin (PeasEngine     *engine,
             PeasPluginInfo *plugin_info)
{
  g_autofree char *icons_dir = NULL;
  GtkIconTheme *icon_theme;
  const char *plugin_datadir;

  g_debug ("Loading plugin: %s", peas_plugin_info_get_name (plugin_info));

  peas_engine_load_plugin (engine, plugin_info);

  /* Add icons */
  plugin_datadir = peas_plugin_info_get_data_dir (plugin_info);

  if (g_str_has_prefix (plugin_datadir, "resource://"))
    plugin_datadir += strlen ("resource://");
  icons_dir = g_strdup_printf ("%s/icons", plugin_datadir);

  icon_theme = gtk_icon_theme_get_for_display (gdk_display_get_default ());
  gtk_icon_theme_add_resource_path (icon_theme, icons_dir);
}


/*
 * GApplication overrides
 */

static void
luminen_application_activate (GApplication *app)
{
	GtkWindow *window;

	g_assert (LUMINEN_IS_APPLICATION (app));

	window = gtk_application_get_active_window (GTK_APPLICATION (app));

	if (window == NULL)
		window = g_object_new (LUMINEN_TYPE_WINDOW,
		                       "application", app,
		                       NULL);

	gtk_window_present (window);
}

static void
luminen_application_startup (GApplication *app)
{
  PeasEngine *engine;

  G_APPLICATION_CLASS (luminen_application_parent_class)->startup (app);

  /* All plugins must be loaded before profiles and Stream Decks */
  engine = peas_engine_get_default ();
  peas_engine_add_search_path (engine,
                               "resource:///com/feaneron/Luminen/plugins",
                               "resource:///com/feaneron/Luminen/plugins");

  for (guint i = 0; i < g_list_model_get_n_items (G_LIST_MODEL (engine)); i++)
    {
      g_autoptr(PeasPluginInfo) info = g_list_model_get_item (G_LIST_MODEL (engine), i);
      load_plugin (engine, info);
    }
}

static void
luminen_application_class_init (LuminenApplicationClass *klass)
{
	GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

	app_class->activate = luminen_application_activate;
	app_class->startup = luminen_application_startup;
}

static void
luminen_application_about_action (GSimpleAction *action,
                                  GVariant      *parameter,
                                  gpointer       user_data)
{
	LuminenApplication *self = user_data;
	GtkWindow *window = NULL;

	g_assert (LUMINEN_IS_APPLICATION (self));

	window = gtk_application_get_active_window (GTK_APPLICATION (self));

	adw_show_about_dialog (GTK_WIDGET (window),
	                       "application-name", "Luminen",
	                       "application-icon", "com.feaneron.Luminen",
	                       "developer-name", "Georges Stavracas",
	                       "version", "0.1.0",
	                       "developers", (const char * const []) {
                           "Georges Basile Stavracas Neto <georges.stavracas@gmail.com>",
                           NULL,
                         },
	                       "designers", (const char * const []) {
                           "Georges Basile Stavracas Neto <georges.stavracas@gmail.com>",
                           NULL,
                         },
	                       "copyright", "© 2023 Georges Basile Stavracas Neto",
	                       NULL);
}

static void
luminen_application_quit_action (GSimpleAction *action,
                                 GVariant      *parameter,
                                 gpointer       user_data)
{
	LuminenApplication *self = user_data;

	g_assert (LUMINEN_IS_APPLICATION (self));

	g_application_quit (G_APPLICATION (self));
}

static const GActionEntry app_actions[] = {
	{ "quit", luminen_application_quit_action },
	{ "about", luminen_application_about_action },
};

static void
luminen_application_init (LuminenApplication *self)
{
	g_action_map_add_action_entries (G_ACTION_MAP (self),
	                                 app_actions,
	                                 G_N_ELEMENTS (app_actions),
	                                 self);
	gtk_application_set_accels_for_action (GTK_APPLICATION (self),
	                                       "app.quit",
	                                       (const char *[]) { "<primary>q", NULL });
}

LuminenApplication *
luminen_application_new (const char        *application_id,
                         GApplicationFlags  flags)
{
	g_return_val_if_fail (application_id != NULL, NULL);

	return g_object_new (LUMINEN_TYPE_APPLICATION,
	                     "application-id", application_id,
	                     "flags", flags,
	                     NULL);
}
