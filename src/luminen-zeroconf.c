/*
 * luminen-zeroconf.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "luminen-light.h"
#include "luminen-zeroconf.h"
#include "luminen-zeroconf-browser.h"

#include <avahi-client/client.h>
#include <avahi-client/lookup.h>
#include <avahi-common/error.h>
#include <avahi-common/timeval.h>
#include <avahi-glib/glib-watch.h>
#include <avahi-glib/glib-malloc.h>
#include <gtk/gtk.h>
#include <libpeas.h>

struct _LuminenZeroconf
{
  GObject parent_instance;

  guint watch_avahi_id;

  PeasExtensionSet *zeroconf_browsers_set;
  AvahiGLibPoll *avahi_poll;
  AvahiClient *client;

  GListStore *zeroconf_browsers_lights;
  GtkFlattenListModel *lights;
};

G_DEFINE_FINAL_TYPE (LuminenZeroconf, luminen_zeroconf, G_TYPE_OBJECT)


/*
 * Callbacks
 */

static void
collect_zeroconf_browser_lights_cb (PeasExtensionSet *set,
                                    PeasPluginInfo   *info,
                                    GObject          *extension,
                                    gpointer          user_data)
{
  LuminenZeroconf *self = LUMINEN_ZEROCONF (user_data);
  GListModel *extension_lights;

  extension_lights = luminen_zeroconf_browser_get_lights (LUMINEN_ZEROCONF_BROWSER (extension));
  g_list_store_append (self->zeroconf_browsers_lights, extension_lights);
}

static void
start_zeroconf_browsers_cb (PeasExtensionSet *set,
                            PeasPluginInfo   *info,
                            GObject          *extension,
                            gpointer          user_data)
{
  LuminenZeroconf *self = LUMINEN_ZEROCONF (user_data);

  luminen_zeroconf_browser_start (LUMINEN_ZEROCONF_BROWSER (extension), self->client);
}

static void
stop_zeroconf_browsers_cb (PeasExtensionSet *set,
                           PeasPluginInfo   *info,
                           GObject          *extension,
                           gpointer          user_data)
{
  luminen_zeroconf_browser_stop (LUMINEN_ZEROCONF_BROWSER (extension));
}

static void
avahi_client_callback_func (AvahiClient      *client,
                            AvahiClientState  state,
                            void             *user_data)
{
  g_debug ("Avahi client state changed: %d", state);
}

static void
on_avahi_vanished_cb (GDBusConnection *connection,
                      const char      *name,
                      gpointer         user_data)
{
  LuminenZeroconf *self = LUMINEN_ZEROCONF (user_data);

  g_debug ("Avahi vanished");

  peas_extension_set_foreach (self->zeroconf_browsers_set, stop_zeroconf_browsers_cb, self);
}

static void
on_avahi_appeared_cb (GDBusConnection *connection,
                      const char      *name,
                      const char      *name_owner,
                      gpointer         user_data)
{
  LuminenZeroconf *self = LUMINEN_ZEROCONF (user_data);
  int error = 0;

  g_debug ("Avahi appeared");

  self->avahi_poll = avahi_glib_poll_new (NULL, G_PRIORITY_DEFAULT);
  self->client = avahi_client_new (avahi_glib_poll_get (self->avahi_poll),
                                   AVAHI_CLIENT_NO_FAIL,
                                   avahi_client_callback_func,
                                   self,
                                   &error);
  if (error != 0)
    {
      g_warning ("Error creating Avahi client: %s", avahi_strerror (error));
      return;
    }

  g_debug ("Avahi version: %s", avahi_client_get_version_string (self->client));

  peas_extension_set_foreach (self->zeroconf_browsers_set, start_zeroconf_browsers_cb, self);
}

static void
luminen_zeroconf_class_init (LuminenZeroconfClass *klass)
{
}

static void
luminen_zeroconf_init (LuminenZeroconf *self)
{
  avahi_set_allocator (avahi_glib_allocator ());

  self->zeroconf_browsers_lights = g_list_store_new (G_TYPE_LIST_MODEL);
  self->lights = gtk_flatten_list_model_new (G_LIST_MODEL (self->zeroconf_browsers_lights));

  self->zeroconf_browsers_set = peas_extension_set_new (peas_engine_get_default (),
                                                        LUMINEN_TYPE_ZEROCONF_BROWSER,
                                                        NULL);
  peas_extension_set_foreach (self->zeroconf_browsers_set,
                              collect_zeroconf_browser_lights_cb,
                              self);

  self->watch_avahi_id = g_bus_watch_name (G_BUS_TYPE_SYSTEM,
                                           "org.freedesktop.Avahi",
                                           G_BUS_NAME_WATCHER_FLAGS_NONE,
                                           on_avahi_appeared_cb,
                                           on_avahi_vanished_cb,
                                           self,
                                           NULL);
}

LuminenZeroconf *
luminen_zeroconf_new (void)
{
  return g_object_new (LUMINEN_TYPE_ZEROCONF, NULL);
}

GListModel *
luminen_zeroconf_get_lights (LuminenZeroconf *self)
{
  g_return_val_if_fail (LUMINEN_IS_ZEROCONF (self), NULL);

  return G_LIST_MODEL (g_object_ref (self->lights));
}
