/* luminen-window.c
 *
 * Copyright 2023 Georges Stavracas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "luminen-light.h"
#include "luminen-window.h"
#include "luminen-zeroconf.h"

struct _LuminenWindow
{
  AdwApplicationWindow parent_instance;

  AdwNavigationPage *controls_page;
  GtkHeaderBar *header_bar;
  AdwBin *light_controller_bin;
  GtkListBox *listbox;
  AdwNavigationView *navigation_view;
  GtkStack *stack;

  GBindingGroup *binding_group;

  LuminenZeroconf *zeroconf;
};

G_DEFINE_FINAL_TYPE (LuminenWindow, luminen_window, ADW_TYPE_APPLICATION_WINDOW)


/*
 * Auxiliary methods
 */

static void
update_visible_page (LuminenWindow *self)
{
  g_autoptr(GListModel) lights = NULL;
  unsigned int n_lights;

  lights = luminen_zeroconf_get_lights (self->zeroconf);
  n_lights = g_list_model_get_n_items (lights);

  gtk_stack_set_visible_child_name (self->stack, n_lights > 0 ? "lights" : "empty");
}


/*
 * Callbacks
 */

static void
on_controls_page_hidden_cb (AdwNavigationPage *page,
                            LuminenWindow     *self)
{
  adw_bin_set_child (self->light_controller_bin, NULL);
}

static void
on_light_row_activated_cb (AdwActionRow  *row,
                           LuminenWindow *self)
{
  LuminenLight *light;
  GtkWidget *controller;

  light = g_object_get_data (G_OBJECT (row), "light");
  g_binding_group_set_source (self->binding_group, light);

  controller = luminen_light_get_controller (light);
  if (g_object_is_floating (controller))
    g_object_ref_sink (controller);
  adw_bin_set_child (self->light_controller_bin, controller);

  adw_navigation_view_push_by_tag (self->navigation_view, "controls");
}

static GtkWidget *
create_light_row_cb (gpointer item,
                     gpointer user_data)
{
  LuminenWindow *self = LUMINEN_WINDOW (user_data);
  GtkWidget *icon;
  GtkWidget *row;

  icon = gtk_image_new_from_icon_name ("go-next-symbolic");

  row = adw_action_row_new ();
  adw_action_row_add_suffix (ADW_ACTION_ROW (row), icon);
  gtk_list_box_row_set_activatable (GTK_LIST_BOX_ROW (row), TRUE);
  g_object_bind_property (item, "name", row, "title", G_BINDING_SYNC_CREATE);
  g_object_set_data_full (G_OBJECT (row), "light", g_object_ref (item), g_object_unref);
  g_signal_connect_object (row, "activated", G_CALLBACK (on_light_row_activated_cb), self, 0);

  return row;
}

static void
on_lights_changed_cb (GListModel    *lights_list,
                      GParamSpec    *pspec,
                      LuminenWindow *self)
{
  update_visible_page (self);
}


/*
 * GObject overrides
 */

static void
luminen_window_finalize (GObject *object)
{
  LuminenWindow *self = LUMINEN_WINDOW (object);

  g_clear_object (&self->zeroconf);

  G_OBJECT_CLASS (luminen_window_parent_class)->finalize (object);
}

static void
luminen_window_class_init (LuminenWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = luminen_window_finalize;

  gtk_widget_class_set_template_from_resource (widget_class, "/com/feaneron/Luminen/luminen-window.ui");
  gtk_widget_class_bind_template_child (widget_class, LuminenWindow, controls_page);
  gtk_widget_class_bind_template_child (widget_class, LuminenWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, LuminenWindow, light_controller_bin);
  gtk_widget_class_bind_template_child (widget_class, LuminenWindow, listbox);
  gtk_widget_class_bind_template_child (widget_class, LuminenWindow, navigation_view);
  gtk_widget_class_bind_template_child (widget_class, LuminenWindow, stack);

  gtk_widget_class_bind_template_callback (widget_class, on_controls_page_hidden_cb);
}

static void
luminen_window_init (LuminenWindow *self)
{
  g_autoptr(GListModel) lights = NULL;

  gtk_widget_init_template (GTK_WIDGET (self));

  self->zeroconf = luminen_zeroconf_new ();

  lights = luminen_zeroconf_get_lights (self->zeroconf);
  gtk_list_box_bind_model (self->listbox, lights, create_light_row_cb, self, NULL);
  g_signal_connect_object (lights, "notify::n-items", G_CALLBACK (on_lights_changed_cb), self, 0);
  update_visible_page (self);

  self->binding_group = g_binding_group_new ();
  g_binding_group_bind (self->binding_group, "name",
                        self->controls_page, "title",
                        G_BINDING_DEFAULT);
}
