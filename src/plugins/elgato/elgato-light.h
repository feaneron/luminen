/*
 * elgato-light.h
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <avahi-common/strlst.h>
#include <gio/gio.h>

G_BEGIN_DECLS

typedef enum
{
  ELGATO_UNKNOWN = -1,
  ELGATO_KEY_LIGHT,
  ELGATO_KEY_LIGHT_AIR,
  ELGATO_KEY_LIGHT_MINI,
  ELGATO_LIGHT_STRIP,
  ELGATO_RING_LIGHT,
} ElgatoLightModel;

#define ELGATO_TYPE_LIGHT (elgato_light_get_type())
G_DECLARE_FINAL_TYPE (ElgatoLight, elgato_light, ELGATO, LIGHT, GObject)

ElgatoLight * elgato_light_new (const char      *name,
                                const char      *address,
                                uint16_t         port,
                                AvahiStringList *txt);

uint32_t elgato_light_get_brightness (ElgatoLight *self);
void elgato_light_set_brightness (ElgatoLight *self,
                                  uint32_t     brightness);

gboolean elgato_light_get_on (ElgatoLight *self);
void elgato_light_set_on (ElgatoLight *self,
                          gboolean     on);

uint32_t elgato_light_get_temperature (ElgatoLight *self);
void elgato_light_set_temperature (ElgatoLight *self,
                                   uint32_t     temperature);

ElgatoLightModel elgato_light_get_model (ElgatoLight *self);

G_END_DECLS
