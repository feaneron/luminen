/*
 * elgato-zeroconf-browser.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "elgato-zeroconf-browser.h"

#include "elgato-light.h"
#include "luminen-light.h"
#include "luminen-zeroconf-browser.h"

#include <avahi-client/lookup.h>

struct _ElgatoZeroconfBrowser
{
  GObject parent_instance;

  AvahiServiceBrowser *service_browser;
  AvahiClient *client;

  GListStore *lights;
};

static void luminen_zeroconf_browser_iface_init (LuminenZeroconfBrowserInterface *iface);

G_DEFINE_FINAL_TYPE_WITH_CODE (ElgatoZeroconfBrowser, elgato_zeroconf_browser, G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE (LUMINEN_TYPE_ZEROCONF_BROWSER,
                                                      luminen_zeroconf_browser_iface_init))

/*
 * Callbacks
 */

static void
service_resolver_callback_func (AvahiServiceResolver   *service_resolver,
                                AvahiIfIndex            interface,
                                AvahiProtocol           protocol,
                                AvahiResolverEvent      event,
                                const char             *name,
                                const char             *type,
                                const char             *domain,
                                const char             *host_name,
                                const AvahiAddress     *address,
                                uint16_t                port,
                                AvahiStringList        *txt,
                                AvahiLookupResultFlags  flags,
                                gpointer                user_data)
{
  ElgatoZeroconfBrowser *self = ELGATO_ZEROCONF_BROWSER (user_data);

  switch (event)
    {
    case AVAHI_RESOLVER_FOUND:
      {
        g_autoptr(ElgatoLight) light = NULL;
        char address_str[AVAHI_ADDRESS_STR_MAX];

        avahi_address_snprint (address_str, sizeof (address_str), address);

        g_debug ("(Resolver) FOUND: service '%s' (%s) at %s:%u", name, type, address_str, port);

        light = elgato_light_new (name, address_str, port, txt);
        g_list_store_append (self->lights, light);
      }
      break;

    case AVAHI_RESOLVER_FAILURE:
      g_debug ("(Resolver) FAILURE: failed to resolve service");

      avahi_service_resolver_new (self->client,
                                  interface,
                                  protocol,
                                  name,
                                  type,
                                  domain,
                                  AVAHI_PROTO_UNSPEC,
                                  0,
                                  service_resolver_callback_func,
                                  self);
      break;

    default:
      g_assert_not_reached ();
    }

  g_clear_pointer (&service_resolver, avahi_service_resolver_free);
}

static void
avahi_service_browser_callback_func (AvahiServiceBrowser    *service_browser,
                                     AvahiIfIndex            interface,
                                     AvahiProtocol           protocol,
                                     AvahiBrowserEvent       event,
                                     const char             *name,
                                     const char             *type,
                                     const char             *domain,
                                     AvahiLookupResultFlags  flags,
                                     gpointer                user_data)
{
  ElgatoZeroconfBrowser *self = ELGATO_ZEROCONF_BROWSER (user_data);

  switch (event)
    {
    case AVAHI_BROWSER_FAILURE:
      g_debug ("(Browser) FAILURE: service '%s' of type '%s' in domain '%s'", name, type, domain);
      break;

    case AVAHI_BROWSER_REMOVE:
      g_debug ("(Browser) REMOVE: service '%s' of type '%s' in domain '%s'", name, type, domain);
      break;

    case AVAHI_BROWSER_NEW:
      g_debug ("(Browser) NEW: service '%s' of type '%s' in domain '%s'", name, type, domain);

      avahi_service_resolver_new (self->client,
                                  interface,
                                  protocol,
                                  name,
                                  type,
                                  domain,
                                  AVAHI_PROTO_UNSPEC,
                                  0,
                                  service_resolver_callback_func,
                                  self);
      break;

    case AVAHI_BROWSER_CACHE_EXHAUSTED:
    case AVAHI_BROWSER_ALL_FOR_NOW:
      break;

    default:
      g_assert_not_reached ();
    }
}


/*
 * LuminenZeroconfBrowser interface
 */

static GListModel *
elgato_zeroconf_browser_get_lights (LuminenZeroconfBrowser *zeroconf_browser)
{
  ElgatoZeroconfBrowser *self = ELGATO_ZEROCONF_BROWSER (zeroconf_browser);

  return G_LIST_MODEL (self->lights);
}

static void
elgato_zeroconf_browser_start (LuminenZeroconfBrowser *zeroconf_browser,
                               AvahiClient            *client)
{
  ElgatoZeroconfBrowser *self = ELGATO_ZEROCONF_BROWSER (zeroconf_browser);

  g_clear_pointer (&self->service_browser, avahi_service_browser_free);

  self->client = client;
  self->service_browser = avahi_service_browser_new (self->client,
                                                     AVAHI_IF_UNSPEC,
                                                     AVAHI_PROTO_INET,
                                                     "_elg._tcp",
                                                     NULL, 0,
                                                     avahi_service_browser_callback_func,
                                                     self);
}

static void
elgato_zeroconf_browser_stop (LuminenZeroconfBrowser *zeroconf_browser)
{
  ElgatoZeroconfBrowser *self = ELGATO_ZEROCONF_BROWSER (zeroconf_browser);

  g_list_store_remove_all (self->lights);

  g_clear_pointer (&self->service_browser, avahi_service_browser_free);
  self->client = NULL;
}

static void
luminen_zeroconf_browser_iface_init (LuminenZeroconfBrowserInterface *iface)
{
  iface->get_lights = elgato_zeroconf_browser_get_lights;
  iface->start = elgato_zeroconf_browser_start;
  iface->stop = elgato_zeroconf_browser_stop;
}


/*
 * GObject overrides
 */

static void
elgato_zeroconf_browser_finalize (GObject *object)
{
  ElgatoZeroconfBrowser *self = (ElgatoZeroconfBrowser *)object;

  g_clear_object (&self->lights);

  G_OBJECT_CLASS (elgato_zeroconf_browser_parent_class)->finalize (object);
}

static void
elgato_zeroconf_browser_class_init (ElgatoZeroconfBrowserClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = elgato_zeroconf_browser_finalize;
}

static void
elgato_zeroconf_browser_init (ElgatoZeroconfBrowser *self)
{
  self->lights = g_list_store_new (LUMINEN_TYPE_LIGHT);
}
