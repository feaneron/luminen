/*
 * elgato-light.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "elgato-light.h"

#include "elgato-light-controller.h"
#include "luminen-light.h"
#include "luminen-soup.h"

#include <json-glib/json-glib.h>
#include <libsoup/soup.h>

struct _ElgatoLight
{
  GObject parent_instance;

  GCancellable *cancellable;
  GUri *uri;

  ElgatoLightModel model;
  gboolean loading;
  gboolean on;
  uint32_t brightness;
  uint32_t temperature;
  char *name;

  GtkWidget *controller;

  uint32_t n_pending_updates;
  gboolean need_update;
};

static void luminen_light_interface_init (LuminenLightInterface *iface);

static void on_lights_state_fetched_cb (GObject      *source_object,
                                        GAsyncResult *result,
                                        gpointer      user_data);

static void on_message_sent_cb (GObject      *source_object,
                                GAsyncResult *result,
                                gpointer      user_data);

G_DEFINE_FINAL_TYPE_WITH_CODE (ElgatoLight, elgato_light, G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE (LUMINEN_TYPE_LIGHT, luminen_light_interface_init))

enum {
  PROP_0,
  PROP_BRIGHTNESS,
  PROP_LOADING,
  PROP_NAME,
  PROP_ON,
  PROP_TEMPERATURE,
  N_PROPS,
};


/*
 * Auxiliary methods
 */

static ElgatoLightModel
parse_model_from_name (const char *name)
{
  if (g_str_has_prefix (name, "Elgato Key Light Air"))
    return ELGATO_KEY_LIGHT_AIR;
  if (g_str_has_prefix (name, "Elgato Key Light Mini"))
    return ELGATO_KEY_LIGHT_MINI;
  if (g_str_has_prefix (name, "Elgato Key Light"))
    return ELGATO_KEY_LIGHT;
  if (g_str_has_prefix (name, "Elgato Light Strip"))
    return ELGATO_LIGHT_STRIP;
  if (g_str_has_prefix (name, "Elgato Ring Light"))
    return ELGATO_RING_LIGHT;

  return ELGATO_UNKNOWN;
}

static void
send_light_state (ElgatoLight *self)
{
  g_autoptr(JsonGenerator) generator = NULL;
  g_autoptr(JsonBuilder) builder = NULL;
  g_autoptr(SoupMessage) message = NULL;
  g_autofree char *json_string = NULL;
  g_autoptr(GBytes) bytes = NULL;
  SoupSession *session;
  size_t json_string_size;

  if (self->n_pending_updates > 0)
    {
      self->need_update = TRUE;
      return;
    }

  builder = json_builder_new ();

  json_builder_begin_object (builder);
    {
      json_builder_set_member_name (builder, "numberOfLights");
      json_builder_add_int_value (builder, 1);

      json_builder_set_member_name (builder, "lights");
      json_builder_begin_array (builder);
        {
          g_autoptr(JsonBuilder) object_builder = NULL;

          object_builder = json_builder_new ();

          json_builder_begin_object (object_builder);
            {
              json_builder_set_member_name (object_builder, "on");
              json_builder_add_int_value (object_builder, self->on ? 1 : 0);

              json_builder_set_member_name (object_builder, "brightness");
              json_builder_add_int_value (object_builder, self->brightness);

              json_builder_set_member_name (object_builder, "temperature");
              json_builder_add_int_value (object_builder, self->temperature);
            }
          json_builder_end_object (object_builder);

          json_builder_add_value (builder, json_builder_get_root (object_builder));
        }
      json_builder_end_array (builder);
    }
  json_builder_end_object (builder);

  generator = json_generator_new ();
  json_generator_set_root (generator, json_builder_get_root (builder));

  json_string = json_generator_to_data (generator, &json_string_size);
  bytes = g_bytes_new_take (g_steal_pointer (&json_string), json_string_size);

  message = soup_message_new_from_uri (SOUP_METHOD_PUT, self->uri);
  soup_message_set_request_body_from_bytes (message, NULL, bytes);

  session = luminen_soup_get_default_session ();
  soup_session_send_async (session,
                           message,
                           G_PRIORITY_HIGH,
                           self->cancellable,
                           on_message_sent_cb,
                           self);

  self->n_pending_updates++;
}

static void
parse_state (ElgatoLight *self,
             GBytes      *bytes)
{
  g_autoptr(JsonParser) parser = NULL;
  g_autoptr(GError) error = NULL;
  gconstpointer data;
  JsonObject *light_object;
  JsonObject *root_object;
  JsonArray *lights_array;
  size_t size;

  data = g_bytes_get_data (bytes, &size);

  parser = json_parser_new ();
  json_parser_load_from_data (parser, data, size, &error);

  if (error)
    {
      g_warning ("Error parsing response from Elgato device: %s", error->message);
      return;
    }

  root_object = json_node_get_object (json_parser_get_root (parser));
  lights_array = json_object_get_array_member (root_object, "lights");
  light_object = json_array_get_object_element (lights_array, 0);

  self->on = json_object_get_int_member (light_object, "on") != 0;
  g_object_notify (G_OBJECT (self), "on");

  self->brightness = json_object_get_int_member (light_object, "brightness");
  g_object_notify (G_OBJECT (self), "brightness");

  self->temperature = json_object_get_int_member (light_object, "temperature");
  g_object_notify (G_OBJECT (self), "temperature");

  g_debug ("Fetched state for '%s': on=%d, brightness=%u, temperature=%u",
           self->name, self->on, self->brightness, self->temperature);
}

static void
fetch_state (ElgatoLight *self)
{
  g_autoptr(SoupMessage) message = NULL;
  SoupSession *session;

  message = soup_message_new_from_uri (SOUP_METHOD_GET, self->uri);

  session = luminen_soup_get_default_session ();
  soup_session_send_and_read_async (session,
                                    message,
                                    G_PRIORITY_HIGH,
                                    self->cancellable,
                                    on_lights_state_fetched_cb,
                                    self);

  self->loading = TRUE;
  g_object_notify (G_OBJECT (self), "loading");
}



/*
 * Callbacks
 */

static void
on_lights_state_fetched_cb (GObject      *source_object,
                            GAsyncResult *result,
                            gpointer      user_data)
{
  g_autoptr(GBytes) bytes = NULL;
  g_autoptr(GError) error = NULL;
  ElgatoLight *self;

  bytes = soup_session_send_and_read_finish (SOUP_SESSION (source_object),
                                             result,
                                             &error);
  if (error)
    {
      if (!g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
        g_warning ("Error loading Elgato light: %s", error->message);
      return;
    }

  self = ELGATO_LIGHT (user_data);

  parse_state (self, bytes);

  self->loading = FALSE;
  g_object_notify (G_OBJECT (self), "loading");
}

static void
on_message_sent_cb (GObject      *source_object,
                    GAsyncResult *result,
                    gpointer      user_data)
{
  g_autoptr(GInputStream) response = NULL;
  g_autoptr(GError) error = NULL;
  ElgatoLight *self;

  response = soup_session_send_finish (SOUP_SESSION (source_object), result, &error);

  if (error)
    {
      if (g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
        return;

      g_warning ("Error updating Elgato light: %s", error->message);
    }

  self = ELGATO_LIGHT (user_data);
  self->n_pending_updates--;

  if (self->need_update)
    {
      self->need_update = FALSE;
      send_light_state (self);
    }
  else if (error)
    {
      fetch_state (self);
    }
}


/*
 * LuminenLight interface
 */

static GtkWidget *
elgato_light_get_controller (LuminenLight *light)
{
  ElgatoLight *self = ELGATO_LIGHT (light);

  if (!self->controller)
    self->controller = elgato_light_controller_new (self);

  return self->controller;
}

static void
luminen_light_interface_init (LuminenLightInterface *iface)
{
  iface->get_controller = elgato_light_get_controller;
}


/*
 * GObject overrides
 */

static void
elgato_light_finalize (GObject *object)
{
  ElgatoLight *self = (ElgatoLight *)object;

  g_cancellable_cancel (self->cancellable);

  g_clear_object (&self->cancellable);
  g_clear_pointer (&self->uri, g_uri_unref);
  g_clear_pointer (&self->name, g_free);

  G_OBJECT_CLASS (elgato_light_parent_class)->finalize (object);
}

static void
elgato_light_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  ElgatoLight *self = ELGATO_LIGHT (object);

  switch (prop_id)
    {
    case PROP_BRIGHTNESS:
      g_value_set_uint (value, self->brightness);
      break;

    case PROP_LOADING:
      g_value_set_boolean (value, self->loading);
      break;

    case PROP_NAME:
      g_value_set_string (value, self->name);
      break;

    case PROP_ON:
      g_value_set_boolean (value, self->on);
      break;

    case PROP_TEMPERATURE:
      g_value_set_uint (value, self->temperature);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
elgato_light_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  ElgatoLight *self = ELGATO_LIGHT (object);

  switch (prop_id)
    {
    case PROP_BRIGHTNESS:
      elgato_light_set_brightness (self, g_value_get_uint (value));
      break;

    case PROP_ON:
      elgato_light_set_on (self, g_value_get_boolean (value));
      break;

    case PROP_TEMPERATURE:
      elgato_light_set_temperature (self, g_value_get_uint (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
elgato_light_class_init (ElgatoLightClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = elgato_light_finalize;
  object_class->get_property = elgato_light_get_property;
  object_class->set_property = elgato_light_set_property;

  g_object_class_install_property (object_class,
                                   PROP_BRIGHTNESS,
                                   g_param_spec_uint ("brightness", "", "",
                                                      0, 100, 0,
                                                      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class,
                                   PROP_LOADING,
                                   g_param_spec_boolean ("loading", "", "",
                                                         TRUE,
                                                         G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_override_property (object_class, PROP_NAME, "name");

  g_object_class_install_property (object_class,
                                   PROP_ON,
                                   g_param_spec_boolean ("on", "", "",
                                                         TRUE,
                                                         G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class,
                                   PROP_TEMPERATURE,
                                   g_param_spec_uint ("temperature", "", "",
                                                      143, 344, 143,
                                                      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
}

static void
elgato_light_init (ElgatoLight *self)
{
  self->cancellable = g_cancellable_new ();
  self->loading = TRUE;
}

ElgatoLight *
elgato_light_new (const char      *name,
                  const char      *address,
                  uint16_t         port,
                  AvahiStringList *txt)
{
  ElgatoLight *self;

  self = g_object_new (ELGATO_TYPE_LIGHT, NULL);
  self->name = g_strdup (name);
  self->model = parse_model_from_name (name);
  self->uri = g_uri_build (G_URI_FLAGS_NONE,
                           "http",
                           NULL,
                           address,
                           port,
                           "/elgato/lights",
                           NULL, NULL);

  fetch_state (self);

  return self;
}

uint32_t
elgato_light_get_brightness (ElgatoLight *self)
{
  g_return_val_if_fail (ELGATO_IS_LIGHT (self), 0);

  return self->brightness;
}

void
elgato_light_set_brightness (ElgatoLight *self,
                             uint32_t     brightness)
{
  g_return_if_fail (ELGATO_IS_LIGHT (self));

  if (self->brightness == brightness)
    return;

  self->brightness = brightness;
  send_light_state (self);
  g_object_notify (G_OBJECT (self), "brightness");
}

gboolean
elgato_light_get_on (ElgatoLight *self)
{
  g_return_val_if_fail (ELGATO_IS_LIGHT (self), FALSE);

  return self->on;
}

void
elgato_light_set_on (ElgatoLight *self,
                     gboolean     on)
{
  g_return_if_fail (ELGATO_IS_LIGHT (self));

  if (self->on == on)
    return;

  self->on = on;
  send_light_state (self);
  g_object_notify (G_OBJECT (self), "on");
}

uint32_t
elgato_light_get_temperature (ElgatoLight *self)
{
  g_return_val_if_fail (ELGATO_IS_LIGHT (self), 0);

  return self->temperature;
}

void
elgato_light_set_temperature (ElgatoLight *self,
                              uint32_t     temperature)
{
  g_return_if_fail (ELGATO_IS_LIGHT (self));

  if (self->temperature == temperature)
    return;

  self->temperature = temperature;
  send_light_state (self);
  g_object_notify (G_OBJECT (self), "temperature");
}
