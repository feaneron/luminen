/*
 * elgato-light-controller.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "elgato-light-controller.h"

struct _ElgatoLightController
{
  AdwBin parent_instance;

  GtkAdjustment *brightness_adjustment;
  GtkSwitch *on_off_switch;
  GtkAdjustment *temperature_adjustment;

  ElgatoLight *light;
};

G_DEFINE_FINAL_TYPE (ElgatoLightController, elgato_light_controller, ADW_TYPE_BIN)

static gboolean
elgato_brightness_to_float (GBinding     *binding,
                            const GValue *from_value,
                            GValue       *to_value,
                            gpointer      user_data)
{
  uint32_t brightness = g_value_get_uint (from_value);

  g_value_set_double (to_value, ((double) brightness) / 100.0);

  return TRUE;
}

static gboolean
float_to_elgato_brightness (GBinding     *binding,
                            const GValue *from_value,
                            GValue       *to_value,
                            gpointer      user_data)
{
  float normalized_brightness = g_value_get_double (from_value);

  g_value_set_uint (to_value, normalized_brightness * 100);

  return TRUE;
}

static gboolean
elgato_temperature_to_float (GBinding     *binding,
                             const GValue *from_value,
                             GValue       *to_value,
                             gpointer      user_data)
{
  uint32_t temperature = g_value_get_uint (from_value);

  g_value_set_double (to_value, ((double) temperature - 143.0) / (344.0 - 143.0));

  return TRUE;
}

static gboolean
float_to_elgato_temperature (GBinding     *binding,
                             const GValue *from_value,
                             GValue       *to_value,
                             gpointer      user_data)
{
  float normalized_brightness = g_value_get_double (from_value);

  g_value_set_uint (to_value, normalized_brightness * 344 + 143);

  return TRUE;
}


static void
elgato_light_controller_class_init (ElgatoLightControllerClass *klass)
{
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

	gtk_widget_class_set_template_from_resource (widget_class, "/com/feaneron/Luminen/plugins/elgato/elgato-light-controller.ui");
  gtk_widget_class_bind_template_child (widget_class, ElgatoLightController, brightness_adjustment);
  gtk_widget_class_bind_template_child (widget_class, ElgatoLightController, on_off_switch);
  gtk_widget_class_bind_template_child (widget_class, ElgatoLightController, temperature_adjustment);
}

static void
elgato_light_controller_init (ElgatoLightController *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}


GtkWidget *
elgato_light_controller_new (ElgatoLight *light)
{
  ElgatoLightController *self;

  self = g_object_new (ELGATO_TYPE_LIGHT_CONTROLLER, NULL);
  self->light = light;

  g_object_bind_property (light, "loading",
                          self, "sensitive",
                          G_BINDING_SYNC_CREATE | G_BINDING_INVERT_BOOLEAN);

  g_object_bind_property (light, "on",
                          self->on_off_switch, "active",
                          G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);

  g_object_bind_property_full (light, "brightness",
                               self->brightness_adjustment, "value",
                               G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                               elgato_brightness_to_float,
                               float_to_elgato_brightness,
                               NULL, NULL);

  g_object_bind_property_full (light, "temperature",
                               self->temperature_adjustment, "value",
                               G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                               elgato_temperature_to_float,
                               float_to_elgato_temperature,
                               NULL, NULL);

  return GTK_WIDGET (self);
}

