/*
 * luminen-zeroconf-browser.h
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>
#include <avahi-client/client.h>

G_BEGIN_DECLS

#define LUMINEN_TYPE_ZEROCONF_BROWSER (luminen_zeroconf_browser_get_type ())
G_DECLARE_INTERFACE (LuminenZeroconfBrowser, luminen_zeroconf_browser, LUMINEN, ZEROCONF_BROWSER, GObject)

struct _LuminenZeroconfBrowserInterface
{
  GTypeInterface parent;

  GListModel * (*get_lights) (LuminenZeroconfBrowser *self);

  void (*start) (LuminenZeroconfBrowser *self,
                 AvahiClient            *client);

  void (*stop) (LuminenZeroconfBrowser *self);
};

GListModel * luminen_zeroconf_browser_get_lights (LuminenZeroconfBrowser *self);

void luminen_zeroconf_browser_start (LuminenZeroconfBrowser *self,
                                     AvahiClient            *client);

void luminen_zeroconf_browser_stop (LuminenZeroconfBrowser *self);

G_END_DECLS
