/*
 * luminen-zeroconf-browser.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "luminen-zeroconf-browser.h"

G_DEFINE_INTERFACE (LuminenZeroconfBrowser, luminen_zeroconf_browser, G_TYPE_OBJECT)

static void
luminen_zeroconf_browser_default_init (LuminenZeroconfBrowserInterface *iface)
{
}

GListModel *
luminen_zeroconf_browser_get_lights (LuminenZeroconfBrowser *self)
{
  g_return_val_if_fail (LUMINEN_IS_ZEROCONF_BROWSER (self), NULL);

  return LUMINEN_ZEROCONF_BROWSER_GET_IFACE (self)->get_lights (self);
}

void
luminen_zeroconf_browser_start (LuminenZeroconfBrowser *self,
                                AvahiClient            *client)
{
  g_return_if_fail (LUMINEN_IS_ZEROCONF_BROWSER (self));

  return LUMINEN_ZEROCONF_BROWSER_GET_IFACE (self)->start (self, client);
}

void
luminen_zeroconf_browser_stop (LuminenZeroconfBrowser *self)
{
  g_return_if_fail (LUMINEN_IS_ZEROCONF_BROWSER (self));

  return LUMINEN_ZEROCONF_BROWSER_GET_IFACE (self)->stop (self);
}
